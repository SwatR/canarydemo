package org.canarydemo.database.remote

import io.reactivex.Observable
import org.canarydemo.mvvm.model.MovieData
import org.canarydemo.utils.others.NetworkConstants
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiInterface {

//    https://api.themoviedb.org/3/movie/popular?api_key=<<api_key>>&language=en-US&page=1

    @GET("popular?")
    fun getMovies(
            @Query("api_key") api_key: String?,
            @Query("language") language: String?,
            @Query("page") page: String?): Observable<Response<MovieData>>
}