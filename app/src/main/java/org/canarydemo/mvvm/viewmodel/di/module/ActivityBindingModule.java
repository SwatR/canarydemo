package org.canarydemo.mvvm.viewmodel.di.module;

import org.canarydemo.mvvm.main.dashboard.DashboardActivity;
import org.canarydemo.mvvm.main.dashboard.MainFragmentBindingModule;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivityBindingModule {

    @ContributesAndroidInjector(modules = {MainFragmentBindingModule.class})
    abstract DashboardActivity bindDashBoardActivity();
}
