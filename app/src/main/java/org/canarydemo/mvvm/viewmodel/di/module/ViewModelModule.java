package org.canarydemo.mvvm.viewmodel.di.module;


import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.ViewModelProvider;

import org.canarydemo.mvvm.main.login.LoginViewModel;
import org.canarydemo.mvvm.main.movie.MovieListViewModel;
import org.canarydemo.mvvm.viewmodel.di.util.ViewModelKey;
import org.canarydemo.mvvm.viewmodel.util.ViewModelFactory;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

@Module
public abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(MovieListViewModel.class)
    abstract AndroidViewModel bindListViewModel(MovieListViewModel listViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(LoginViewModel.class)
    abstract AndroidViewModel bindDetailsViewModel(LoginViewModel detailsViewModel);

    @Binds
    abstract ViewModelProvider.Factory bindViewModelFactory(ViewModelFactory factory);
}
