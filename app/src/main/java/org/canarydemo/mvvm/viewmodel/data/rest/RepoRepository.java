package org.canarydemo.mvvm.viewmodel.data.rest;

import org.canarydemo.database.remote.ApiInterface;
import org.canarydemo.mvvm.model.MovieData;

import javax.inject.Inject;

import io.reactivex.Observable;
import retrofit2.Response;

public class RepoRepository {

    private final ApiInterface repoService;

    @Inject
    public RepoRepository(ApiInterface repoService) {
        this.repoService = repoService;
    }

    public Observable<Response<MovieData>> getMovies(String apiKey, String lang, String page) {
        return repoService.getMovies(apiKey, lang, page);
    }

}
