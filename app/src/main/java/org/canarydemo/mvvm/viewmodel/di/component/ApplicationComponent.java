package org.canarydemo.mvvm.viewmodel.di.component;

import android.app.Application;

import org.canarydemo.mvvm.base.BaseApplication;
import org.canarydemo.mvvm.viewmodel.di.module.ActivityBindingModule;
import org.canarydemo.mvvm.viewmodel.di.module.ApplicationModule;
import org.canarydemo.mvvm.viewmodel.di.module.ContextModule;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;
import dagger.android.support.DaggerApplication;

@Singleton
@Component(modules = {ContextModule.class, ApplicationModule.class, AndroidSupportInjectionModule.class, ActivityBindingModule.class})
public interface ApplicationComponent extends AndroidInjector<DaggerApplication> {

    void inject(BaseApplication application);

    @Component.Builder
    interface Builder {
        @BindsInstance
        Builder application(Application application);

        ApplicationComponent build();
    }
}