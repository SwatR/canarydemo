package me.cook.cook4me.ui.auth.login

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.ViewModelProvider
import kotlinx.android.synthetic.main.fragment_login.*
import org.canarydemo.R
import org.canarydemo.mvvm.base.BaseFragment
import org.canarydemo.mvvm.main.login.LoginViewModel
import org.canarydemo.mvvm.main.movie.MovieListFragment

/**
 * Created by Swati Rajawat.
 */

class LoginFragment : BaseFragment<LoginViewModel>() {

    var rootView: View? = null
    private lateinit var loginViewModel: LoginViewModel
    override fun getViewModel(): LoginViewModel {
        loginViewModel = ViewModelProvider(requireActivity()).get(LoginViewModel::class.java)
        return loginViewModel
    }

    companion object {
        @JvmStatic
        fun newInstance() = LoginFragment()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.fragment_login, container, false)
        }
        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeResponse()
        btn_login.setOnClickListener(object : View.OnClickListener {
            override fun onClick(view: View?) {
                if (isNetworkConnected())
                    loginViewModel.loginUser(email.text.toString(), password.text.toString())
                else
                    Toast.makeText(requireActivity(), "Connect to internet", Toast.LENGTH_SHORT).show()

            }

        })
    }

    private fun observeResponse() {
        loginViewModel.error.observeForever {
            showError(it!!)
        }
        loginViewModel.redirectToHome.observeForever {
            if (it == true) {

                val manager: FragmentManager = requireActivity().supportFragmentManager
                val transaction: FragmentTransaction = manager.beginTransaction()
                        .replace(R.id.fragmentContainer, MovieListFragment.newInstance())
                transaction.commit()

            }
        }
    }

    private fun showError(error: String) {
        Toast.makeText(requireActivity(), error, Toast.LENGTH_LONG).show()

    }
}