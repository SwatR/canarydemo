package org.canarydemo.mvvm.main.movie

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import org.canarydemo.R
import org.canarydemo.mvvm.model.ResultsItem


class MovieListAdapter internal constructor(context: Context) :
    RecyclerView.Adapter<MovieListAdapter.WordViewHolder>() {
    private val inflater: LayoutInflater = LayoutInflater.from(context)
    private var city = emptyList<ResultsItem>()
    private var mContext: Context = context

    inner class WordViewHolder(itemView: View) :
        androidx.recyclerview.widget.RecyclerView.ViewHolder(
            itemView
        ) {
        var cityItemView: TextView = itemView.findViewById(R.id.tvTitle)
        var profileItemView: ImageView = itemView.findViewById(R.id.thumbnail)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WordViewHolder {
        val itemView = inflater.inflate(R.layout.movie_list_item, parent, false)
        return WordViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: WordViewHolder, position: Int) {
        val current = city[position]
        holder.cityItemView.text = current.title?.trim()
        Picasso.get()
            .load("https://image.tmdb.org/t/p/w200/" + current.poster_path)
            .fit()
            .centerCrop()
            .error(R.drawable.city)
            .into(holder.profileItemView);

    }

    internal fun setMovies(words: List<ResultsItem?>?) {
        this.city = words as List<ResultsItem>
        notifyDataSetChanged()
    }

    override fun getItemCount() = city.size

}