package org.canarydemo.mvvm.main.dashboard;

import org.canarydemo.mvvm.main.movie.MovieListFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import me.cook.cook4me.ui.auth.login.LoginFragment;

@Module
public abstract class MainFragmentBindingModule {

    @ContributesAndroidInjector
    abstract LoginFragment provideListFragment();

    @ContributesAndroidInjector
    abstract MovieListFragment provideDetailsFragment();
}
