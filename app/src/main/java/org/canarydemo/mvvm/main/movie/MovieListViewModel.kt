package org.canarydemo.mvvm.main.movie

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers
import org.canarydemo.database.remote.RetrofitService
import org.canarydemo.mvvm.model.MovieData
import org.canarydemo.mvvm.model.ResultsItem
import org.canarydemo.mvvm.viewmodel.data.rest.RepoRepository
import org.canarydemo.utils.others.NetworkConstants
import retrofit2.Response
import javax.inject.Inject

class MovieListViewModel(application: Application) : AndroidViewModel(application) {

    lateinit var retrofitService: RetrofitService
    var weatherResponseLiveData = MutableLiveData<MovieData>()
    var mMoviesLivedata = MutableLiveData<List<ResultsItem>>()
    var loading = MutableLiveData<Boolean>()
    private var compositeDisposable = CompositeDisposable()

    @Inject
    lateinit var mRepoRepository: RepoRepository

    init {
        retrofitService = RetrofitService()

    }

    fun getMoviesList() {
        loading.value = true
        val loginDisposable = retrofitService.apiInterface.getMovies(
            NetworkConstants.API_KEY,
            "en-US",
            "1"
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeWith(object : DisposableObserver<Response<MovieData>>() {
                override fun onComplete() {
                    loading.value = false
                }

                override fun onNext(value: Response<MovieData>) {
                    Log.i("MovieData", "movielist : " + value.body()?.results)
                    mMoviesLivedata.value = value.body()?.results

                }

                override fun onError(e: Throwable) {
                    loading.value = false
                }
            })
        compositeDisposable.add(loginDisposable)

    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }

}