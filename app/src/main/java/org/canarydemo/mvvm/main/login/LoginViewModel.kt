package org.canarydemo.mvvm.main.login

import android.app.Application
import android.util.Patterns
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData


/**
 * Created by Swati Rajawat.
 */

class LoginViewModel(application: Application) : AndroidViewModel(application) {

    var error = MutableLiveData<String>()
    var redirectToHome = MutableLiveData<Boolean>()


    fun loginUser(email: String, password: String) {

        if (email.isEmpty() && password.isEmpty()) {
            error.value = "Please enter user ID and password!"
            return
        }

        if (!email.isValidEmail()) {
            error.value = "Please enter valid username"
            return
        }
        if (password.isEmpty()) {
            error.value = "Please enter your password"
            return
        }
        if (password.length<8 || password.length>15) {
            error.value = "Password length should be greater than 8 and less than 15"
            return
        }
        redirectToHome.value = true

    }

    fun CharSequence?.isValidEmail() = !isNullOrEmpty() && Patterns.EMAIL_ADDRESS.matcher(this).matches()

}