package org.canarydemo.mvvm.main.movie

import android.app.Dialog
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import org.canarydemo.R
import org.canarydemo.mvvm.base.BaseFragment
import org.canarydemo.mvvm.main.movie.MovieListViewModel_MembersInjector.create
import org.canarydemo.mvvm.model.MovieData
import org.canarydemo.mvvm.viewmodel.data.rest.RepoRepository_Factory.create
import org.canarydemo.mvvm.viewmodel.di.component.DaggerApplicationComponent
import org.canarydemo.mvvm.viewmodel.util.ViewModelFactory
import javax.inject.Inject

/*Screen to show Movie list info*/

class MovieListFragment : BaseFragment<MovieListViewModel>() {

    lateinit var movieListViewModel: MovieListViewModel
    var rootView: View? = null
    private lateinit var layoutManager: RecyclerView.LayoutManager
    private lateinit var progressDialog: Dialog

    companion object {
        fun newInstance(): MovieListFragment = MovieListFragment()
    }

    override fun getViewModel(): MovieListViewModel {
        movieListViewModel =
            ViewModelProvider(requireActivity()).get(MovieListViewModel::class.java)
        return movieListViewModel


//        movieListViewModel=  ViewModelProvider.AndroidViewModelFactory(activity!!.application)
//            .create(MovieListViewModel::class.java)
//        return movieListViewModel
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (!isNetworkConnected()) {
            rootView = inflater.inflate(R.layout.no_internet_fragment, container, false)

        } else {
            rootView = inflater.inflate(R.layout.fragment_city, container, false)
            setRetainInstance(true)
            initInstances(rootView)
        }

        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

    private fun initializeProgressLoader() {
        progressDialog = Dialog(requireActivity())
        progressDialog.setCancelable(false)
        progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        progressDialog.window?.setBackgroundDrawableResource(android.R.color.transparent)
        progressDialog.setContentView(R.layout.dialog_progress)

        movieListViewModel.loading.value = false
    }

    private fun observeLoading() {
        movieListViewModel.loading.observeForever({
            showProgressLoader(it!!)
        })
    }

    private fun showProgressLoader(visible: Boolean) {
        if (visible) {
            progressDialog.show()
        } else {
            progressDialog.cancel()
        }
    }

    fun initInstances(view: View?) {
        movieListViewModel = ViewModelProvider(this).get(MovieListViewModel::class.java)

        initializeProgressLoader()
        observeLoading()
        val adapter = MovieListAdapter(requireContext())
        layoutManager = GridLayoutManager(requireContext(), 2)
        val recycler_view = view?.findViewById(R.id.recycler_view) as RecyclerView
        recycler_view.layoutManager = layoutManager
        recycler_view.adapter = adapter

        movieListViewModel.mMoviesLivedata.observe(requireActivity(), Observer { cities ->
            cities?.let { adapter.setMovies(it) }
        })
        showProgressLoader(true)
        movieListViewModel.getMoviesList()
        movieListViewModel.weatherResponseLiveData.observe(
            requireActivity(),
            object : Observer<MovieData> {
                override fun onChanged(movieData: MovieData?) {
                    Log.d("movieData", "" + movieData)
                    if (movieData != null) {
                        if (view != null) {
                            adapter.setMovies(movieData.results)
                            showProgressLoader(false)
                        }

                    }

                }
            }
        );

    }

}