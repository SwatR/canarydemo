package org.canarydemo.mvvm.main.dashboard

import android.os.Bundle
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.ViewModelProvider
import dagger.android.AndroidInjector
import me.cook.cook4me.ui.auth.login.LoginFragment
import org.canarydemo.R
import org.canarydemo.mvvm.base.BaseActivity


class DashboardActivity : BaseActivity<DashboardViewModel>() {

    private lateinit var dashboardViewModel: DashboardViewModel

    override fun getViewModel(): DashboardViewModel {
        dashboardViewModel = ViewModelProvider(this).get(DashboardViewModel::class.java)
        return dashboardViewModel
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard)

        if (savedInstanceState == null) {
            val manager: FragmentManager = supportFragmentManager
            val transaction: FragmentTransaction = manager.beginTransaction()
                .add(R.id.fragmentContainer, LoginFragment.newInstance())
            transaction.commit()
        }


    }

}
